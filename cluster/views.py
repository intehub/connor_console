from django.shortcuts import render
from cluster.k8s.dashboard import Dashboard, Infra
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required


# Create your views here.

@login_required(login_url='/login.html')
def clustermgt(request):
	dashboard = Dashboard()
	aggr_res = dashboard.get_resources_summary()
	context = {
		'aggr_res_deployments': aggr_res['deployments'],
		'aggr_res_replicasets': aggr_res['replicasets'],
		'aggr_res_pods': aggr_res['pods']}
	return render(request, 'ClusterMgt.html', context)


def get_namespaces(request):
	namespaces = Infra().get_namespaces()
	internal_namespaces = ['default', 'kube-system']
	for namespace in internal_namespaces:
		namespaces.remove(namespace)
	
	data = {'success': 'true', 'namespaces': namespaces}
	
	return JsonResponse(data)
