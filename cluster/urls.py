from django.conf.urls import url

from . import views

app_name = 'cluster'

urlpatterns = [
    url(r'^ClusterMgt.html$', views.clustermgt, name='clustermgt'),
    url(r'^get_namespaces$', views.get_namespaces, name='get_namespaces')
]
