from __future__ import print_function
from cluster.k8s.conf import Infra
import pykube
import os
import json


class Dashboard:
    def __init__(self):
        self.k8s_cluster_config = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../config/k8s/config'))
        self.api = pykube.HTTPClient(pykube.KubeConfig.from_file(self.k8s_cluster_config))
        self.namespaces = Infra().get_namespaces()
        # print (self.namespaces)

    def get_resources_summary(self):
        self.aggr_res = {}

        self.deployments_list = []
        for self.namespace in self.namespaces:
            self.res_dpl = pykube.Deployment.objects(self.api).filter(namespace=self.namespace).query_cache['response']
            # print (self.res_dpl['items'])
            if self.res_dpl['items']:
                for self.item in self.res_dpl['items']:
                    self.elements_dlp = {
                        "name": self.item['metadata']['name'],
                        "service": self.item['metadata']['labels']['app'],
                        "namespace": self.namespace,
                        "labels": self.item['metadata']['labels'],
                        "time": self.item['metadata']['creationTimestamp']}
                    self.deployments_list.append(self.elements_dlp)
        # print (self.deployments_list)

        self.replicasets_list = []
        for self.namespace in self.namespaces:
            self.res_rs = pykube.ReplicaSet.objects(self.api).filter(namespace=self.namespace).query_cache['response']
            if self.res_rs['items']:
                # print (self.res_rs)
                for self.item in self.res_rs['items']:
                    self.elements_rs = {
                        "name": self.item['metadata']['name'],
                        "service": self.item['metadata']['labels']['app'],
                        "namespace": self.namespace, "labels": self.item['metadata']['labels'],
                        "time": self.item['metadata']['creationTimestamp'],
                        "ready_pods": self.item['status']['replicas'],
                        # "desired_pods": self.item['spec']['metadata']['replicas'],
                        "images": self.item['spec']['template']['spec']['containers'][0]['image']
                    }
                    self.replicasets_list.append(self.elements_rs)
        # print (self.replicasets_list)

        self.pods_list = []
        for self.namespace in self.namespaces:
            self.res_pods = pykube.Pod.objects(self.api).filter(namespace=self.namespace).query_cache['response']
            print(self.res_pods['items'])
            if self.res_pods['items']:
                for self.item in self.res_pods['items']:
                    self.elements_pods = {
                        "name": self.item['metadata']['name'],
                        "service": self.item['metadata']['name'],
                        "namespace": self.namespace,
                        "time": self.item['metadata']['creationTimestamp'],
                        "status": self.item['status']['phase'],
                        # "restart_counts": self.item['status']['containerStatuses'][0]['restartCount'],
                        # "host": self.item['status']['hostIP']
                    }
                    self.pods_list.append(self.elements_pods)
        # print (self.pods_list)

        self.aggr_res['deployments'] = self.deployments_list
        self.aggr_res['replicasets'] = self.replicasets_list
        self.aggr_res['pods'] = self.pods_list

        # print (self.aggr_res)
        return self.aggr_res

    def get_resources_details(self):
        pass
