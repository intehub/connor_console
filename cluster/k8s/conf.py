from __future__ import print_function
import pykube
import os
import json


class Infra:
    def __init__(self):
        self.k8s_cluster_config = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../config/k8s/config'))
        self.api = pykube.HTTPClient(pykube.KubeConfig.from_file(self.k8s_cluster_config))
        self.namespaces = self.get_namespaces()

    def get_nodes(self):
        self.node_res = pykube.Node.objects(self.api).query_cache['response']
        print(self.node_res)

    def get_namespaces(self):
        self.arr_namespace = []
        self.namespace_res = pykube.Namespace.objects(self.api).query_cache['response']
        for self.namespace in self.namespace_res['items']:
            self.arr_namespace.append(self.namespace['metadata']['name'])
        return self.arr_namespace
