from __future__ import print_function
import json
import _jsonnet

jsonnet_str = '''
{
  "apiVersion": "extensions/v1beta1",
  "kind": "Deployment",
  "metadata": {
    "name": std.extVar("stack_name"),
    "namespace": std.extVar("namespace")
  },
  "spec": {
    "replicas": 5,
    "template": {
      "metadata": {
        "labels": {
          "app": std.extVar("stack_name")
        }
      },
      "spec": {
        "containers": [
          {
            "name": std.extVar("stack_name"),
            "image": "nginx:1.7.9",
            "ports": [
              {"containerPort": 80}
            ]
          }
        ]
      }
    }
  }
}
'''

json_str = _jsonnet.evaluate_snippet("snippet", jsonnet_str, ext_vars={'stack_name': 'adviva-api', 'namespace': 'prod'})
json_obj = json.loads(json_str)
#for person_id, person in json_obj.iteritems():
#    print('%s is %s, greeted by "%s"' % (person_id, person['name'], person['welcome']))
print(json_obj



stop_win_range = np.arange(2.0, 4.5, 0.5) stop_loss_range = np.arange(0.5, 2, 0.5) sell_atr_nstop_factor_grid = { 'class': [AbuFactorAtrNStop], 'stop_loss_n' : stop_loss_range, 'stop_win_n' : stop_win_range } print('AbuFactorAtrNStop止盈参数stop_win_n设置范围:{}'.format(stop_win_range)) print('AbuFactorAtrNStop止损参数stop_loss_n设置范围:{}'.format(stop_loss_range))
