from django.conf.urls import url

from . import views

app_name = 'deployer'

urlpatterns = [
    url(r'^AppSetup.html$', views.service_setup, name='service_setup'),
    url(r'^app_save$', views.app_save, name='app_save'),
    url(r'^AppList.html$', views.app_list, name='app_list'),
    url(r'^api/applist', views.api_app_list, name='api_app_list'),
    url(r'^ReleaseTask.html$', views.release_task, name='release_task'),
    url(r'^AppDetail/(?P<service_name>[a-zA-Z\-0-9]+)$', views.app_detail, name='app_detail'),
    url(r'^tasks/create/$', views.cd_task_creation, name='cd_task_creation'),
    url(r'^tasks/rr-p1/$', views.rolling_release_p1, name='rr-p1'),
    url(r'^tasks/rr-p2/$', views.rolling_release_p2, name='rr-p2'),
    url(r'^tasks/rb/$', views.rollback, name='rb'),
    url(r'^tasks/ack/$', views.acknowledge, name='ack'),
    url(r'^watch/$', views.watch, name='watch')
]
