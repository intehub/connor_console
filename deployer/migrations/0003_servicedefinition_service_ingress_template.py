# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2018-02-21 17:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('deployer', '0002_auto_20171112_2023'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicedefinition',
            name='service_ingress_template',
            field=models.CharField(default='', max_length=200),
        ),
    ]
