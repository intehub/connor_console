from __future__ import unicode_literals

from django.db import models
from projects.models import ServiceDefinition as pjServiceDefinition
from django.utils.encoding import python_2_unicode_compatible
import datetime


# Create your models here.
@python_2_unicode_compatible
class DeployTasks(models.Model):
    task_id = models.AutoField(primary_key=True)
    ci_task_id = models.IntegerField(default=0)
    service_id = models.IntegerField(default=0)
    username = models.CharField(max_length=30, default='0')
    namespace = models.CharField(max_length=30, default='default')
    service_name = models.CharField(max_length=30, default='default')
    is_init_provisioned = models.IntegerField(default=0)
    is_full_provisioned = models.IntegerField(default=0)
    is_deployed = models.IntegerField(default=0)
    is_rolledback = models.IntegerField(default=0)
    is_autotask = models.IntegerField(default=0)
    task_createtime = models.DateTimeField(auto_now_add=True)
    deployed_datetime = models.DateTimeField(null=True)
    rollback_datetime = models.DateTimeField(null=True)
    service_selector_md5 = models.CharField(max_length=200, default='default')
    service_selector_text = models.CharField(max_length=200, default='default')
    service_deploy_tmpl = models.CharField(max_length=200)
    service_port = models.CharField(max_length=5, default='0')
    release_tag = models.CharField(max_length=200, default='not-defined')
    rollback_tag = models.CharField(max_length=200, default='not-defined')
    stack_deploy_tmpl = models.CharField(max_length=300)
    container_name = models.CharField(max_length=30, default='default')
    container_image = models.CharField(max_length=200, default='none')
    container_resource_cpu = models.CharField(max_length=20, default='BestEffort')
    container_resource_mem = models.CharField(max_length=20, default='BestEffort')
    container_port = models.CharField(max_length=5, default='0')
    init_replica = models.CharField(max_length=3, default='0')
    desired_replica = models.CharField(max_length=3, default='0')

    def __str__(self):
        return self.username

    def set_flags(self, task_id, op, value):
        self.task_id = task_id
        self.op = op
        self.value = value
        self.rs = DeployTasks.objects.get(task_id=self.task_id)
        if op == 'ack':
            self.rs.is_deployed = self.value
            self.rs.deployed_datetime = datetime.datetime.now()
            return self.rs.save()
        elif op == 'is_init_provisioned':
            self.rs.is_init_provisioned = self.value
            self.rs.task_createtime = datetime.datetime.now()
            return self.rs.save()
        elif op == 'is_full_provisioned':
            self.rs.is_full_provisioned = self.value
            return self.rs.save()
        elif op == 'is_rolledback':
            self.rs.is_rolledback = self.value
            self.rs.rollback_datetime = datetime.datetime.now()
            return self.rs.save()
        else:
            print ('Invalid Operation')
            return False


@python_2_unicode_compatible
class ServiceDefinition(models.Model):
    service = models.OneToOneField(pjServiceDefinition, on_delete=models.CASCADE, primary_key=True)
    service_name = models.CharField(max_length=30, default='0')
    namespace = models.CharField(max_length=30, default='0')
    service_desc = models.CharField(max_length=500, default='')
    service_port = models.CharField(max_length=5, default='0')
    service_label = models.CharField(max_length=300, default='')
    service_ingress_template = models.CharField(max_length=200, default='')
    service_deploy_template = models.CharField(max_length=200)
    stack_deploy_template = models.CharField(max_length=200)
    container_resource_cpu = models.CharField(max_length=20, default='BestEffort')
    container_resource_mem = models.CharField(max_length=20, default='BestEffort')
    is_autotask = models.IntegerField(default=0)
    init_replica = models.IntegerField(default=1)
    desired_replica = models.IntegerField(default=1)
    service_owner = models.CharField(max_length=50, default='')
    service_ops_approver = models.CharField(max_length=100, default='')
    service_ops_approver_email = models.CharField(max_length=500, default='')
    jenkins_job_name = models.CharField(max_length=200, default='')
    
    def app_def_save(self, service_id, service_name, namespace, service_port, service_label, service_deploy_template,
                     stack_deploy_template, container_resource_cpu, container_resource_mem, is_autotask, init_replica,
                     desired_replica, service_desc, service_ops_approver, service_ops_approver_email, service_owner,
                     jenkins_job_name):
        self.record = ServiceDefinition(service_id=service_id, service_name=service_name, namespace=namespace,
                                        service_port=service_port, service_label=service_label,
                                        service_deploy_template=service_deploy_template,
                                        stack_deploy_template=stack_deploy_template,
                                        container_resource_cpu=container_resource_cpu,
                                        container_resource_mem=container_resource_mem, is_autotask=is_autotask,
                                        init_replica=init_replica, desired_replica=desired_replica,
                                        service_desc=service_desc,service_ops_approver=service_ops_approver,
                                        service_ops_approver_email=service_ops_approver_email,
                                        service_owner=service_owner, jenkins_job_name=jenkins_job_name
                                        )
        self.record.save()
        if self.record.service_id:
            return True
        else:
            return False

    def __str__(self):
        return self.service_name