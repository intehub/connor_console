# -*- coding: utf-8 -*-
import json
from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.core import serializers
from deployer.models import DeployTasks, ServiceDefinition
from deployer.k8s.deploy import StackDeployer
from deployer.k8s.conf import Config
from projects.conf import CIConfig
from projects.models import ServiceDefinition as pjServiceDefinition
from projects.models import BuildTasks as pjBuildTasks
from cluster.k8s.conf import Infra


@login_required(login_url='/login.html')
def service_setup(request):
    # get k8s deploy template file list
    file_list = Config().get_k8s_template_list()

    # get namespace list from current k8s cluster as the selection of deployment environments
    namespace = Infra().get_namespaces()

    context = {'loops': range(1, 101), 'template': file_list, 'namespace': namespace}
    return render(request, 'AppSetup.html', context)


@login_required(login_url='/login.html')
@csrf_exempt
def app_save(request):
    print('Raw Data: "%s"' % request.body)
    if request.method == 'POST':
        # List all the parameters needed for application addition
        appdef_data = json.loads(request.body)
        try:
            service_name = appdef_data['service_name']
            service_po = appdef_data['pd_principal']
            service_po_email = appdef_data['pd_principal_email']
            service_qa = appdef_data['qa_principal']
            service_qa_email = appdef_data['qa_principal_email']
            service_desc = appdef_data['service_desc']
            service_port = appdef_data['service_port']
            service_template = appdef_data['service_template']
            deployment_template = appdef_data['deployment_template']
            cpu_limit = appdef_data['cpu_limit']
            mem_limit = appdef_data['mem_limit']
            init_replica = appdef_data['init_replica']
            desired_replica = appdef_data['desired_replica']
            env_vars = appdef_data['env_vars']
            # print (env_vars)

            current_release_tag = ''

            ciconfig = CIConfig()
            for env in env_vars:
                namespace = env['env_name']
                if ciconfig.service_n_namespace_exists(service_name, namespace):
                    print('服务和对应部署环境已存在，跳过创建此环境')
                    return False
                else:
                    is_autotask = env['is_autotask']
                    service_label = json.dumps(env['service_tags'])
                    jenkins_url = env['jenkins_url']
                    jenkins_jobname = env['jenkins_jobname']

                    # save in parent table - projects
                    project_save_record = pjServiceDefinition()
                    project_service_id = project_save_record.app_def_save(service_name, namespace, jenkins_url,
                                                                          jenkins_jobname, is_autotask,
                                                                          current_release_tag,
                                                                          service_po, service_po_email,
                                                                          service_qa, service_qa_email,
                                                                          )
                    
                    # save in child table - deployer
                    deployer_save_record = ServiceDefinition()
                    deployer_save_record.app_def_save(project_service_id, service_name, namespace, service_port,
                                                      service_label, service_template, deployment_template, cpu_limit,
                                                      mem_limit, is_autotask, init_replica, desired_replica,
                                                      service_desc, service_po, service_po_email, service_po,
                                                      jenkins_jobname)

            return HttpResponse(True)
        except KeyError:
            return HttpResponse("Malformed data!")

    elif request.method == 'GET':
        return HttpResponse('welcome!')


@login_required(login_url='/login.html')
def app_list(request):
    if request.method == 'POST':
        return HttpResponse("Malformed data!")
    elif request.method == 'GET':
        return render(request, 'AppList.html')


@csrf_exempt
# @login_required(login_url='/login.html')
def api_app_list(request):
    context = {}
    services_list_by_name = {}
    services_list_by_id = {}
    
    services_envs = pjServiceDefinition.objects.all().values('service_name').annotate(env_count=Count('service_name'))
    services_ids = pjServiceDefinition.objects.values_list('id', flat=True).order_by('-id')
    services_names = pjServiceDefinition.objects.values_list('service_name', flat=True).distinct().order_by('service_name')
    
    for service_name in services_names:
        services_list_by_name[service_name] = pjServiceDefinition.objects.filter(service_name=service_name).order_by('id')
    
    for service_id in services_ids:
        if pjBuildTasks.objects.filter(service_id=service_id).order_by('-release_tag')[:1]:
            services_list_by_id[service_id] = pjBuildTasks.objects.filter(service_id=service_id).order_by('-release_tag')[:1]
        else:
            services_list_by_id[service_id] = [{}]
            
    context['services_list_by_name'] = list(services_list_by_name)
    context['services_list_by_id'] = list(services_list_by_id)
    context['services_envs'] = list(services_envs)
    context['services_names'] = list(services_names)
    
    # context = {'services_list_by_name': services_list_by_name, 'services_list_by_id': services_list_by_id,
    #            'services_envs': services_envs, 'services_names': services_names}
    
    print (context)
    return HttpResponse(JsonResponse(context), content_type="application/json")


@login_required(login_url='/login.html')
def app_detail(request, service_name):
    service_name = service_name
    release_info = {}
    
    service_detail = ServiceDefinition.objects.select_related('service').filter(service_name=service_name)
    service_envs = pjServiceDefinition.objects.values_list('namespace', flat=True).filter(service_name=service_name)
    for env in service_envs:
        release_info[env] = DeployTasks.objects.filter(service_name=service_name, namespace=env).order_by('-task_id')
    
    context = {'service_detail': service_detail, 'release_info': release_info}
    return render(request, 'AppDetail.html', context)


@login_required(login_url='/login.html')
def release_task(request):
    username = request.user
    ci_tasks = {}
    ci_tasks_json = {}
    ci_service_names = []
    ci_service_envs = {}
    cd_tasks = {}
    
    # get all the services owned by a user
    ci_services = CIConfig().get_ci_service_by_owner(username)
    ci_services_json = [{'id': item.id, 'service_name': item.service_name, 'namespace': item.namespace,
                         'jenkins_url': item.jenkins_url, 'job_name': item.job_name, 'is_autotask': item.is_autotask,
                         'current_release_tag': item.current_release_tag, 'service_po': item.service_po,
                         'service_po_email': item.service_po_email, 'service_qa': item.service_qa,
                         'service_qa_email': item.service_qa_email
                         } for item in ci_services]
    
    # get related CI data structure
    for service in ci_services:
        # get CI task for each service (latest 10 CI tasks Only)
        ci_tasks[service.id] = CIConfig().get_ci_task_by_service_id(service.id)
        # get usable rollback tag (current release or previous successful release, code 200)
        for item in ci_tasks[service.id]:
            if item.status == 1:
                ci_tasks_json.setdefault(service.id, []).append({'id': item.id, 'release_tag': item.release_tag,
                                                                 'rollback_tag': item.rollback_tag,
                                                                 'po_approved': item.po_approved,
                                                                 'qa_approved': item.qa_approved, 'status': item.status,
                                                                 'service_id': item.service_id,
                                                                 })
                
        # get uniq service name list
        if service.service_name not in ci_service_names:
            ci_service_names.append(service.service_name)
        # get env list for each service
        ci_service_envs.setdefault(service.service_name, []).append({service.id: service.namespace})
        
        # organize deploy task by ci task id
        for ci_task in ci_tasks[service.id]:
            cd_tasks[ci_task.id] = Config().get_cd_tasks_by_ci_task_id(ci_task.id)
    
    context = {'ci_services': ci_services, 'ci_services_json': json.dumps(ci_services_json),
               'ci_tasks': ci_tasks, 'ci_tasks_json': json.dumps(ci_tasks_json),
               'ci_service_names': ci_service_names, 'ci_service_envs': json.dumps(ci_service_envs),
               'cd_tasks': cd_tasks}
    
    return render(request, 'ReleaseTask.html', context)


@csrf_exempt
def cd_task_creation(request):
    if request.method == 'POST':
        print ('Raw Data: "%s"' % request.body)
        deploy_data = json.loads(request.body)
        build_task_id = deploy_data['build_task_id']
        try:
            deployer = StackDeployer()
            return HttpResponse(deployer.task_creation(build_task_id))
        except KeyError:
            return HttpResponse("Malformed data!")
    elif request.method == 'GET':
        return HttpResponse('welcome!')


@csrf_exempt
def rolling_release_p1(request):
    if request.method == 'POST':
        print ('Raw Data: "%s"' % request.body)
        deploy_data = json.loads(request.body)
        deploy_task_id = deploy_data['deploy_task_id']
        try:
            deployer = StackDeployer()
            return HttpResponse(deployer.init_deployment(deploy_task_id))
        except KeyError:
            return HttpResponse("Malformed data!")
    elif request.method == 'GET':
        return HttpResponse('welcome!')


@login_required(login_url='/login.html')
@csrf_exempt
def rolling_release_p2(request):
    if request.method == 'POST':
        deploy_data = json.loads(request.body)
        try:
            deploy_task_id = deploy_data['deploy_task_id']
            deployer = StackDeployer()
            return HttpResponse(deployer.worker(deploy_task_id))
        except KeyError:
            return HttpResponse("Malformed data!")
    elif request.method == 'GET':
        return HttpResponse('welcome!')


@login_required(login_url='/login.html')
@csrf_exempt
def rollback(request):
    if request.method == 'POST':
        deploy_data = json.loads(request.body)
        try:
            deploy_task_id = deploy_data['deploy_task_id']
            deployer = StackDeployer()
            return HttpResponse(deployer.service_rollback(deploy_task_id))
        except KeyError:
            return HttpResponse("Malformed data!")
    elif request.method == 'GET':
        return HttpResponse('welcome!')


@login_required(login_url='/login.html')
@csrf_exempt
def acknowledge(request):
    if request.method == 'POST':
        deploy_data = json.loads(request.body)
        try:
            deploy_task_id = deploy_data['deploy_task_id']
            deployer = StackDeployer()
            return HttpResponse(deployer.set_task_status(deploy_task_id, 'ack', 1))
        except KeyError:
            return HttpResponse("Malformed data!")
    elif request.method == 'GET':
        return HttpResponse('welcome!')

@login_required(login_url='/login.html')
@csrf_exempt
def watch(request):
    deploy = StackDeployer()
    return HttpResponse(deploy.watch())
