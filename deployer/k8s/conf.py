from __future__ import print_function
from deployer.models import DeployTasks, ServiceDefinition
import pykube
import json
import os


class Config:
    def __init__(self):
        # Init pykube client
        self.k8s_cluster_config = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../config/k8s/config'))
        self.api = pykube.HTTPClient(pykube.KubeConfig.from_file(self.k8s_cluster_config))

        # Set deploy code path
        self.deploy_code_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'config/'))

        self.res = ''
        self.service_name = ''

    def get_task_list(self):
        self.res_task_list = DeployTasks.objects.all()
        return self.res_task_list

    def get_task_list_by_servicename(self, service_name, namespace):
        self.service_name = service_name
        self.namespace = namespace
        self.res_task_list = DeployTasks.objects.filter(service_name=self.service_name, namespace=self.namespace)
        return self.res_task_list

    def get_cd_task_by_task_id(self, task_id):
        self.task_id = task_id
        self.res_get_cd_task = DeployTasks.objects.filter(task_id=self.task_id)
        
        if self.res_get_cd_task and len(self.res_get_cd_task) == 1:
            return self.res_get_cd_task
        else:
            return False
        
    def get_cd_tasks_by_ci_task_id(self, ci_task_id):
        self.ci_task_id = ci_task_id
        self.res = DeployTasks.objects.filter(ci_task_id=self.ci_task_id)
        return self.res
    
    # Currently, I have to assume last successfully deployed service as the current running service, we might need to
    # replace this assumption with other more accurate factors e.g. information from CI part
    def get_current_service_version(self, namespace, service_name):
        self.namespace = namespace
        self.service_name = service_name
        self.res = ServiceDefinition.objects.filter(service_name=self.service_name, namespace=self.namespace).prefetch_related(
            'stacks').order_by('-service_id')[:1]
        
        if self.res:
            return self.res[0].release_tag
        else:
            return False

    # Get service summary for overview
    def get_service_summary(self):
        self.res_service_list = ServiceDefinition.objects.all()
        return self.res_service_list

    def get_service_config_by_serviceid(self, service_id):
        self.service_id = service_id
        self.res_service_detail = ServiceDefinition.objects.filter(service_id=self.service_id)

        return {
            'service_name': self.res_service_detail[0].service_name,
            'namespace': self.res_service_detail[0].namespace,
            'service_port': self.res_service_detail[0].service_port,
            'service_label': self.res_service_detail[0].service_label,
            'service_ingress_template': self.res_service_detail[0].service_ingress_template,
            'service_deploy_template': self.res_service_detail[0].service_deploy_template,
            'stack_deploy_template': self.res_service_detail[0].stack_deploy_template,
            'container_resource_cpu': self.res_service_detail[0].container_resource_cpu,
            'container_resource_mem': self.res_service_detail[0].container_resource_mem,
            'init_replica': self.res_service_detail[0].init_replica,
            'desired_replica': self.res_service_detail[0].desired_replica,
            'service_desc': self.res_service_detail[0].service_desc,
            'service_ops_approver': self.res_service_detail[0].service_ops_approver,
            'service_ops_approver_email': self.res_service_detail[0].service_ops_approver_email,
            'service_owner': self.res_service_detail[0].service_owner,
            'jenkins_job_name': self.res_service_detail[0].jenkins_job_name
        }

    # get stack config of a service instance by task_id, which would be commonly used
    def get_service_config(self, task_id, is_autotask, resource_existence_status):
        self.task_id = task_id
        self.autotask = is_autotask
        self.resource_existence_status = resource_existence_status
        self.res_service_config = DeployTasks.objects.filter(task_id=self.task_id)

        # If it's an auto task OR a service previously not existing at all, we will set init replica to desired replica
        # straightaway here
        if self.autotask == 1 or self.resource_existence_status == '1':
            self.init_replica = self.res_service_config[0].desired_replica
        else:
            self.init_replica = self.res_service_config[0].init_replica

        if self.res_service_config and len(self.res_service_config) == 1:
            return {
                'service_name': self.res_service_config[0].service_name,
                'service_labels': self.res_service_config[0].service_selector_text,
                'service_deploy_name': self.res_service_config[0].release_tag,
                'service_deploy_tmpl': self.res_service_config[0].service_deploy_tmpl,
                'service_selector_text': self.res_service_config[0].service_selector_text,
                'service_selector_md5': self.res_service_config[0].service_selector_md5,
                'service_port': self.res_service_config[0].service_port,
                'stack_name': self.res_service_config[0].service_name,
                'stack_deploy_name': self.res_service_config[0].release_tag,
                'stack_labels_text': self.res_service_config[0].service_selector_text,
                'stack_labels_md5': self.res_service_config[0].service_selector_md5,
                'stack_deploy_tmpl': self.res_service_config[0].stack_deploy_tmpl,
                'container_name': self.res_service_config[0].container_name,
                'container_image': self.res_service_config[0].container_image,
                'container_resource_cpu': self.res_service_config[0].container_resource_cpu,
                'container_resource_mem': self.res_service_config[0].container_resource_mem,
                'container_port': self.res_service_config[0].container_port,
                'init_replica': self.init_replica,
                'desired_replica': self.res_service_config[0].desired_replica,
                'release_tag': self.res_service_config[0].release_tag
            }
        else:
            return False

    def get_k8s_template_list(self):
        self.path = self.deploy_code_path
        self.file_list = os.listdir(self.path)
        return self.file_list
