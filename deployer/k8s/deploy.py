# coding:utf-8
from __future__ import print_function
import datetime
from deployer.models import DeployTasks, ServiceDefinition
from projects.conf import CIConfig
from deployer.k8s.conf import Config
from registrar.ingress import ServiceManifests
from django.http import JsonResponse
import pykube
import os
import json
import _jsonnet
import hashlib


class StackDeployer:
    def __init__(self):
        # Set deploy code path
        self.deploy_code_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'config/'))
        
        # Init pykube client
        self.k8s_cluster_config = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../config/k8s/config'))
        self.api = pykube.HTTPClient(pykube.KubeConfig.from_file(self.k8s_cluster_config))
        
        # repo_url
        self.repo_url = '172.31.6.147:5000'
        
        # Init Config Class
        self.config = Config()
        self.ciconfig = CIConfig()
        
        # Init Ingress Interface
        self.ingress = ServiceManifests()
        
        # Others
        self.release_prefix = 'release-'
        self.rollback_prefix = 'rollback-'

    def worker(self, task_id):
        self.deploy_task_id = task_id
        self.deploy_task_res = self.config.get_cd_task_by_task_id(self.deploy_task_id)
        
        # Init required parameters
        self.ci_task_id = self.deploy_task_res[0].ci_task_id
        self.service_id = self.deploy_task_res[0].service_id
        self.service_name = self.deploy_task_res[0].service_name
        self.namespace = self.deploy_task_res[0].namespace
        self.release_tag = self.deploy_task_res[0].release_tag
        self.rollback_tag = self.deploy_task_res[0].rollback_tag
        self.is_autotask = self.deploy_task_res[0].is_autotask
        self.selector_token = self.deploy_task_res[0].service_selector_md5
        self.is_init_provisioned = self.deploy_task_res[0].is_init_provisioned
        self.is_full_provisioned = self.deploy_task_res[0].is_full_provisioned
        self.is_deployed = self.deploy_task_res[0].is_deployed
        self.is_rolledback = self.deploy_task_res[0].is_rolledback
        
        self.task_approval = CIConfig().get_ci_tasks_by_task_id(self.ci_task_id)
        
        # Setup Ingress Rule Name
        self.rollback_ingress_rule_name = self.rollback_prefix + self.service_name
        
        # 1 - Check if the release task was approved by QA before commencing
        if self.task_approval['qa_approved'] == 0 or self.task_approval['qa_approved'] == 2:
            print ('Invalid Operations - Task Is Not Approved By QA Yet')
            self.ret = {'success': 'false', 'msg': u'操作失败 - 产品QA尚未审批或已拒绝此发布任务'}
            return JsonResponse(self.ret)
        
        # 2 - Only one running deployment task is allowed
        if self.release_still_running(self.service_id, task_id=self.deploy_task_id):
            print ('Invalid Operations - There Is Release Still Running')
            self.ret = {'success': 'false', 'msg': u'操作失败 - 目前有其他发布任务尚未结束'}
            return JsonResponse(self.ret)
        
        # 3 - Verify the existence status of the service and fill the template
        self.resource_existence_status = str(self.resource_exists(self.namespace, self.service_name, self.selector_token))
        self.json_obj_map = self.fill_template(self.namespace, self.deploy_task_id, self.service_name, self.is_autotask, self.resource_existence_status)
        
        # Perform full provision
        # Condition Desc:
        # resource_existence_status = 1 AND init provision was completed AND full provision is NOT started yet
        if self.resource_existence_status == '1' and self.is_init_provisioned == 1 and self.is_full_provisioned == 0:
            # Full Deployment - Step 1: Scale Up Specified Service To Desired Replica
            if self.service_update(self.namespace, self.service_name, self.selector_token, self.json_obj_map):
                print ('Scaling Up To Desired Replica for Service: ' + self.service_name + ' In Namespace: ' + self.namespace + ' Successful')
            else:
                print ('Scaling Up To Desired Replica for Service: ' + self.service_name + ' In Namespace: ' + self.namespace + ' Failed')
                self.ret = {'success': 'false', 'msg': u'操作失败 - 未能成功扩容至指定实例数'}
                return JsonResponse(self.ret)

            # Full Deployment - Step 2: Update "Rollback-Ingress" Rule To Allow Rollback Action When Needed
            if self.ingress.worker(self.service_id, self.rollback_ingress_rule_name, self.rollback_tag):
                print ('Creation/Update Of - Rollback - Ingress Rule Of Service: ' + self.service_name + ' For Release: ' + self.release_tag + ' Successful, Rollback Version: ' + self.rollback_tag)
            else:
                print ('Creation/Update Of - Rollback - Ingress Rule Of Service: ' + self.service_name +' For Release: ' + self.release_tag + ' Failed, Rollback Version: ' + self.rollback_tag)
                self.ret = {'success': 'false', 'msg': u'操作失败 - 更新回滚Ingress规则失败'}
                return JsonResponse(self.ret)
            
            # Full Deployment - Step 3: Switch To New Release Version, In This Step, New Release Will Be Switched On
            if self.ingress.worker(self.service_id, self.service_name, self.release_tag):
                print ('Creation/Update Of - Master - Ingress Rule Of Service: ' + self.service_name + ' For Release: ' + self.release_tag + ' Successful, Rollback Version: ' + self.rollback_tag)
            else:
                print ('Creation/Update Of - Master - Ingress Rule Of Service: ' + self.service_name + ' For Release: ' + self.release_tag + ' Failed, Rollback Version: ' + self.rollback_tag)
                self.ret = {'success': 'false', 'msg': u'操作失败 - 切换至待发布版本操作失败'}
                return JsonResponse(self.ret)
            
            # Full Deployment - Step 4: Update Release Status, Indicating That Full Deployment Was Done Successfully
            if self.set_task_status(self.task_id, 'is_full_provisioned', 1):
                print ('Release Operation For Version: ' + self.release_tag + ' Successful')
                self.ret = {'success': 'true', 'msg': u'操作成功'}
                return JsonResponse(self.ret)
            else:
                print ('Release Operation For Version: ' + self.release_tag + ' Failed')
                self.ret = {'success': 'false', 'msg': u'操作失败 - 更新发布状态失败'}
                return JsonResponse(self.ret)
        else:
            print ('Invalid Operations - Unknown Error')
            self.ret = {'success': 'false', 'msg': u'操作失败 - 非法操作，请检查当前任务状态是否正确'}
            return JsonResponse(self.ret)
    
    def resource_exists(self, namespace, service_name, selector_token):
        self.namespace = namespace
        self.service_name = service_name
        self.selector_token = selector_token
        
        self.service_exists = pykube.Deployment.objects(self.api).filter(namespace=self.namespace, selector={"app__eq": self.service_name, "token__eq": self.selector_token},)
        print(self.service_exists)
        self.new_release_token = pykube.Deployment.objects(self.api).filter(namespace=self.namespace, selector={"app__eq": self.service_name, "token__neq": self.selector_token},)
        
        if self.service_exists:
            # already exists, cannot do release again, ignoring this wrong request
            print ('Service name: ' + self.service_name + ' with selector token: ' + self.selector_token + ' already existed.')
            return 1
        else:
            if self.new_release_token:
                # Found new release token, normally that means new release is coming
                print ('Service name: ' + self.service_name + ' exists, NEW selector token: ' + self.selector_token + ' detected. Rolling release will be triggered')
                return 2
            else:
                # No service and stacks detected
                print ('Service name: ' + self.service_name + ' with selector token: ' + self.selector_token + ' not existing.')
                return 0
    
    # service_creation will include k8s service and deployment creation, etcd external lb key creation/update
    def service_creation(self, service_id, namespace, service_name, selector_token, ingress_rule_name, release_tag, json_obj_map):
        self.service_id = service_id
        self.namespace = namespace
        self.service_name = service_name
        self.selector_token = selector_token
        self.ingress_rule_name = ingress_rule_name
        self.release_tag = release_tag
        self.json_obj_map = json_obj_map
        
        # Step1: create services and deployments
        self.service_creation_res = pykube.Service(self.api, self.json_obj_map['service']).create()
        self.stack_creation_res = pykube.Deployment(self.api, self.json_obj_map['stack']).create()
        
        if str(self.service_creation_res) == 'None' and str(self.stack_creation_res) == 'None':
            print ('Service name: ' + self.service_name + ' with selector token: ' + self.selector_token + ' creation successful.')
        else:
            print ('Service name: ' + self.service_name + ' with selector token: ' + self.selector_token + ' creation failed, rolling back.')
            self.service_deletion(self.namespace, self.service_name, self.selector_token, self.json_obj_map, self.release_tag)
            return False
        
        # Step2: create/update ingress entries
        self.create_ingress_rule_ret = self.ingress.worker(self.service_id, self.ingress_rule_name, self.release_tag)
        if self.create_ingress_rule_ret:
            print ('success')
            return True
        else:
            print ('fail')
            return False
    
    def service_deletion(self, namespace, service_name, selector_token, json_obj_map, release_tag):
        self.namespace = namespace
        self.service_name = service_name
        self.selector_token = selector_token
        self.json_obj_map = json_obj_map
        self.release_tag = release_tag
        
        # Delete services and deployments
        self.service_deletion_res = pykube.Service(self.api, self.json_obj_map['service']).delete()
        self.stack_deletion_res = pykube.Deployment(self.api, self.json_obj_map['stack']).delete()
        
        if str(self.service_deletion_res) == 'None' and str(self.stack_deletion_res) == 'None':
            print ('Service name: ' + self.service_name + ' with selector token: ' + self.selector_token + ' deletion successful.')
            return True
        else:
            print ('Service name: ' + self.service_name + ' with selector token: ' + self.selector_token + ' deletion failed.')
            return False
    
    def service_rollback(self, task_id):
        self.task_id = task_id
        self.task_details = self.config.get_cd_task_by_task_id(self.task_id)
        
        # Init required parameters
        self.service_name = self.task_details[0].service_name
        self.namespace = self.task_details[0].namespace
        self.release_tag = self.task_details[0].release_tag
        self.rollback_tag = self.task_details[0].rollback_tag
        self.is_autotask = self.task_details[0].is_autotask
        self.selector_token = self.task_details[0].service_selector_md5
        self.is_init_provisioned = self.task_details[0].is_init_provisioned
        self.is_full_provisioned = self.task_details[0].is_full_provisioned
        self.is_deployed = self.task_details[0].is_deployed
        self.is_rolledback = self.task_details[0].is_rolledback
        self.service_id = self.task_details[0].service_id
        
        # Verify the existence status of the service
        self.resource_existence_status = str(self.resource_exists(self.namespace, self.service_name, self.selector_token))
        
        # get the whole config map
        self.json_obj_map = self.fill_template(self.namespace, self.task_id, self.service_name, self.is_autotask, self.resource_existence_status)

        # Rollback Deployment - Step 1: Switch Ingress Rule To Specified Version
        if self.ingress.worker(self.service_id, self.service_name, self.rollback_tag):
            print('Creation/Update Of - Master - Ingress Rule Of Service: ' + self.service_name + ' To Rollback Version: ' + self.rollback_tag + ' Successful')
        else:
            print('Creation/Update Of - Master - Ingress Rule Of Service: ' + self.service_name + ' To Rollback Version: ' + self.rollback_tag + ' Failed')
            self.ret = {'success': 'false', 'msg': u'操作失败 - 切换至回滚版本操作失败'}
            return JsonResponse(self.ret)

        # Rollback Deployment - Step 2: Clean Up Service And Deployment Of The Version Been Rolled Back
        # Rollback Deployment - Step 3: Update Task Database Status
        if self.resource_existence_status == '1':
            if self.service_deletion(self.namespace, self.service_name, self.selector_token, self.json_obj_map, self.release_tag) and self.set_task_status(self.task_id, 'is_rolledback', 1):
                print ('rolling back successful')
                self.ret = {'success': 'true', 'msg': u'操作成功'}
                return JsonResponse(self.ret)
            else:
                print ('rolling back failed')
                self.ret = {'success': 'false', 'msg': u'操作失败 - 切换至回滚版本操作失败'}
                return JsonResponse(self.ret)
        else:
            print ('Fatal: no existing services for deletion')
            self.ret = {'success': 'false', 'msg': u'操作失败 - 无法回滚不存在的服务'}
            return JsonResponse(self.ret)
    
    def service_update(self, namespace, service_name, selector_token, json_obj_map):
        self.namespace = namespace
        self.service_name = service_name
        self.selector_token = selector_token
        self.json_obj_map = json_obj_map
        
        # Step1: Update services and deployments
        self.service_update_res = pykube.Service(self.api, self.json_obj_map['service']).update()
        self.stack_update_res = pykube.Deployment(self.api, self.json_obj_map['stack']).update()

        if str(self.service_update_res) == 'None' and str(self.stack_update_res) == 'None':
            return True
        else:
            return False
    
    def fill_template(self, namespace, task_id, service_name, is_autotask, resource_existence_status):
        self.template_config_map = {}
        self.namespace = namespace
        self.task_id = task_id
        self.service_name = service_name
        self.is_autotask = is_autotask
        self.resource_existence_status = resource_existence_status
        
        # get service and stack configs by service name
        self.config_map = self.config.get_service_config(self.task_id, self.is_autotask, self.resource_existence_status)
        
        self.service_script = os.path.join(self.deploy_code_path, self.config_map['service_deploy_tmpl'])
        self.stack_script = os.path.join(self.deploy_code_path, self.config_map['stack_deploy_tmpl'])
        
        # append namespace into hash map
        self.config_map['namespace'] = self.namespace
        
        with open(self.service_script, 'r') as service_file:
            self.service_jsonnet_str = service_file.read()
            self.service_json_str = _jsonnet.evaluate_snippet("snippet", self.service_jsonnet_str, ext_vars=self.config_map)
            self.service_json_obj = json.loads(self.service_json_str)
        
        with open(self.stack_script, 'r') as stack_file:
            self.stack_jsonnet_str = stack_file.read()
            self.stack_json_str = _jsonnet.evaluate_snippet("snippet", self.stack_jsonnet_str, ext_vars=self.config_map)
            self.stack_json_obj = json.loads(self.stack_json_str)
        
        self.template_config_map['service'] = self.service_json_obj
        self.template_config_map['stack'] = self.stack_json_obj
        
        return self.template_config_map
    
    def set_task_status(self, task_id, op, value):
        self.task_id = task_id
        self.op = op
        self.value = value
        
        self.delete_theotherkey = ServiceManifests()
        
        self.task_res = DeployTasks()
        self.task_details = self.config.get_cd_task_by_task_id(self.task_id)
        
        self.is_init_provisioned = self.task_details[0].is_init_provisioned
        self.is_full_provisioned = self.task_details[0].is_full_provisioned
        self.is_rolledback = self.task_details[0].is_rolledback
        self.release_tag = self.task_details[0].release_tag
        self.service_name = self.task_details[0].service_name
        self.namespace = self.task_details[0].namespace
        
        # Not allowing to click confirm to finalize the release before init & full provisioning were completed
        if (self.is_init_provisioned == 0 or self.is_full_provisioned == 0) and self.op == 'ack':
            print ('Not allowing to click confirm to finalize the release before init/full provisioning were completed')
            self.ret = {'success': 'false', 'msg': u'操作失败 - 尚未完成初始化或完整部署'}
            return JsonResponse(self.ret)
        
        if self.is_rolledback == 1 and self.op == 'ack':
            print ('Not allowing to click confirm to finalize the release if it\'s a rolled back task')
            self.ret = {'success': 'false', 'msg': u'操作失败 - 此任务已回滚'}
            return JsonResponse(self.ret)
        
        # Set flags
        if str(self.task_res.set_flags(self.task_id, self.op, self.value)) == 'None':
            print ('Successfully set the flag')
            self.ret = {'success': 'true', 'msg': u'操作成功'}
            return JsonResponse(self.ret)
        else:
            print('Failed to set flag')
            self.ret = {'success': 'false', 'msg': u'操作失败'}
            return JsonResponse(self.ret)
    
    # This is to ensure that there's only one release process running at the same time for a specific project
    def release_still_running(self, service_id, task_id=False):
        self.service_id = service_id
        self.task_id = task_id
        
        if self.task_id:
            self.res = DeployTasks.objects.filter(service_id=self.service_id, is_deployed=0).exclude(task_id=self.task_id).exclude(is_rolledback=1)
        else:
            self.res = DeployTasks.objects.filter(service_id=self.service_id, is_deployed=0).exclude(is_rolledback=1)
        
        if self.res:
            return True
        else:
            return False
    
    def task_creation(self, build_task_id):
        # Init parameters from jenkins
        self.build_task_id = build_task_id
        
        # get the other parameters from database via build task id
        self.res_build_task = self.ciconfig.get_ci_tasks_by_task_id(self.build_task_id)

        self.service_id = self.res_build_task['service_id']
        self.release_tag = self.res_build_task['release_tag']
        self.rollback_tag = self.res_build_task['rollback_tag']
        self.po_approved = self.res_build_task['po_approved']
        self.qa_approved = self.res_build_task['qa_approved']
        self.status = self.res_build_task['status']
        
        if str(self.rollback_tag) == 'None':
            self.rollback_tag = 'not-defined'
            
        if self.release_still_running(self.service_id):
            print ('Insertion Of Deployment Task Failed - There Is Release In Progress')
            return False

        self.res_svc_def = ServiceDefinition.objects.filter(service_id=self.service_id)
        self.service_name = self.res_svc_def[0].service_name
        self.namespace = self.res_svc_def[0].namespace
        self.is_autotask = self.res_svc_def[0].is_autotask

        self.image = self.repo_url + '/' + self.release_tag

        # Instant replacement of version information against label release_tag and convert to md5 for further usage
        self.json_service_label = json.loads(self.res_svc_def[0].service_label)
        self.json_service_label['version'] = self.release_tag
        self.str_service_label = json.dumps(self.json_service_label)

        # get md5 of labels
        self.m = hashlib.md5()
        self.m.update(self.str_service_label)
        self.selector_token = self.m.hexdigest()
        
        # Create task record
        self.insert_deploytask = DeployTasks(
            namespace=self.namespace, service_name=self.service_name,
            is_init_provisioned=0, is_full_provisioned=0, is_deployed=0, is_rolledback=0,
            task_createtime=datetime.datetime.now(), service_selector_md5=self.selector_token,
            service_selector_text=self.str_service_label,
            service_deploy_tmpl=self.res_svc_def[0].service_deploy_template,
            service_port=self.res_svc_def[0].service_port, release_tag=self.release_tag,
            rollback_tag=self.rollback_tag, stack_deploy_tmpl=self.res_svc_def[0].stack_deploy_template,
            container_name=self.service_name, container_image=self.image,
            container_resource_cpu=self.res_svc_def[0].container_resource_cpu,
            container_resource_mem=self.res_svc_def[0].container_resource_mem,
            container_port=self.res_svc_def[0].service_port,
            init_replica=self.res_svc_def[0].init_replica, desired_replica=self.res_svc_def[0].desired_replica,
            is_autotask=self.is_autotask, ci_task_id=self.build_task_id, service_id=self.service_id
        )
        self.insert_deploytask.save()
        self.deploy_task_id = self.insert_deploytask.task_id
        if not self.deploy_task_id:
            print('Insertion Of Deployment Task Failed.')
            return False
        else:
            print ('Insertion Of Deployment Task Successful')
            return True
    
    def init_deployment(self, deploy_task_id):
        # Init parameters from jenkins
        self.deploy_task_id = deploy_task_id
        
        self.res_deploy_task = Config().get_cd_task_by_task_id(deploy_task_id)
        self.service_name = self.res_deploy_task[0].service_name
        self.namespace = self.res_deploy_task[0].namespace
        self.is_autotask = self.res_deploy_task[0].is_autotask
        self.ci_task_id = self.res_deploy_task[0].ci_task_id
        
        # Setup ingress rule name
        self.release_ingress_rule_name = self.release_prefix + self.service_name
        
        # get the other parameters from database via build task id
        self.res_build_task = self.ciconfig.get_ci_tasks_by_task_id(self.ci_task_id)
        
        self.service_id = self.res_build_task['service_id']
        self.release_tag = self.res_build_task['release_tag']
        self.rollback_tag = self.res_build_task['rollback_tag']
        self.po_approved = self.res_build_task['po_approved']
        self.qa_approved = self.res_build_task['qa_approved']
        self.status = self.res_build_task['status']
        
        self.res_svc_def = ServiceDefinition.objects.filter(service_id=self.service_id)
        
        self.image = self.repo_url + '/' + self.release_tag
        
        # Instant replacement of version information against label release_tag and convert to md5 for further usage
        self.json_service_label = json.loads(self.res_svc_def[0].service_label)
        self.json_service_label['version'] = self.release_tag
        self.str_service_label = json.dumps(self.json_service_label)
        
        # get md5 of labels
        self.m = hashlib.md5()
        self.m.update(self.str_service_label)
        self.selector_token = self.m.hexdigest()
        
        # 1 - Ensure the service definition is correct
        if not self.res_svc_def or len(self.res_svc_def) > 1:
            print('Service Not Defined OR Definition Error')
            return False
        
        # 2 - Check if the release task was approved by Product Owner before commencing
        if self.po_approved == 0 or self.po_approved == 2:
            print ('Invalid Operations - Task Is Not Approved By Product Owner Yet')
            self.ret = {'success': 'false', 'msg': u'操作失败 - 产品负责人尚未审批或已拒绝此发布任务'}
            return JsonResponse(self.ret)
        
        # 3 - Only one running deployment task is allowed
        if self.release_still_running(self.service_id, task_id=self.deploy_task_id):
            print ('Invalid Operations - There Is Release Still Running')
            self.ret = {'success': 'false', 'msg': u'操作失败 - 目前有其他发布任务正在进行中'}
            return JsonResponse(self.ret)
        
        # 4 - Verify the existence status of the service and fill the template
        self.resource_existence_status = str(self.resource_exists(self.namespace, self.service_name, self.selector_token))
        if self.resource_existence_status == '1':
            print ('Invalid Operations - Service With Same Selector Token Already Existed')
            self.ret = {'success': 'false', 'msg': u'操作失败 - 集群中已有相同服务'}
            return JsonResponse(self.ret)
        
        self.json_obj_map = self.fill_template(self.namespace, self.deploy_task_id, self.service_name, self.is_autotask,
                                               self.resource_existence_status)
        
        # Perform init provision (Condition desc: it requires resource_existence_status != 0 or 2
        # AND init provision has NOT been performed yet AND full provision is NOT started yet)
        if self.service_creation(self.service_id, self.namespace, self.service_name, self.selector_token, self.release_ingress_rule_name, self.release_tag, self.json_obj_map):
            self.set_task_status(self.task_id, 'is_init_provisioned', 1)
            print ('New service ' + self.service_name + ' created.')
            self.ret = {'success': 'true', 'msg': u'操作成功'}
            return JsonResponse(self.ret)
        else:
            print ('New service ' + self.service_name + ' failed to be created.')
            self.ret = {'success': 'false', 'msg': u'操作失败'}
            return JsonResponse(self.ret)
