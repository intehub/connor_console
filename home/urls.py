from django.conf.urls import url
from . import views

app_name = 'home'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^index.html$', views.index, name='index'),
    url(r'^HelpCenter.html$', views.helpcenter, name='helpcenter'),
    url(r'^PermissionMgt.html$', views.permissionmgt, name='permissionmgt'),
    url(r'^login.html$', views.login_page, name='login_page'),
    url(r'^login$', views.auth_user, name='auth_user'),
    url(r'^add_user$', views.add_user, name='add_user'),
    url(r'^logout$', views.logout_view, name='logout_view'),
    url(r'^my/ReleaseApproval.html$', views.release_approval, name='release_approval'),
]
