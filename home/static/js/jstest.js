function post_ci_request(id, namespace, result)
{
var xmlhttp;
if (window.XMLHttpRequest)
  { // code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp = new XMLHttpRequest();
  }
else
  { // code for IE6, IE5
  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }

var parameters = "namespace=" + namespace + "&service_id=" + id;

xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    if (xmlhttp.responseText == "True") {
        document.getElementById(result).innerHTML="已发送";
    } else {
        document.getElementById(result).innerHTML="发送失败";
    }
    } else {
        document.getElementById(result).innerHTML="发送失败";
    }
  }

xmlhttp.open("POST", "/build/request", true);
// xmlhttp.setRequestHeader("X-CSRF-TOKEN", csrftoken);
xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xmlhttp.send(parameters);
}


function execute_ci_request(id, namespace, result)
{
var xmlhttp;
if (window.XMLHttpRequest)
  { // code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp = new XMLHttpRequest();
  }
else
  { // code for IE6, IE5
  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }

var select_element_id_release = id + "_release";
var select_element_id_rollback = id + "_rollback";

selected_release_tag = document.getElementById(select_element_id_release).value;
selected_rollback_tag = document.getElementById(select_element_id_rollback).value;

alert(selected_release_tag);

var parameters = "namespace=" + namespace + "&service_id=" + id + "&task_id=" + selected_release_tag + "&rollback_id=" + selected_rollback_tag;
alert(parameters);

xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
       {
          if (xmlhttp.responseText == "True")
          {
             document.getElementById(result).innerHTML="构建任务已完成";
          }
       }
    else if (xmlhttp.readyState==1)
       {
          document.getElementById(result).innerHTML="构建任务已下发，请稍后..";
       }
    else
       {
          document.getElementById(result).innerHTML="发送失败";
       }
  }


xmlhttp.open("POST", "/build/execute", true);
// xmlhttp.setRequestHeader("X-CSRF-TOKEN", csrftoken);
xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xmlhttp.send(parameters);
}

function deploy_action(task_id, post_url, result)
{
var xmlhttp;
if (window.XMLHttpRequest)
  { // code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp = new XMLHttpRequest();
  }
else
  { // code for IE6, IE5
  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }

var parameters = "task_id=" + task_id;
// alert(parameters);
// alert(post_url);

xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
       {
          if (xmlhttp.responseText == "True")
          {
             document.getElementById(result).innerHTML="已执行";
          }
       }
    else if (xmlhttp.readyState==1)
       {
          document.getElementById(result).innerHTML="部署任务已下发，请稍后..";
       }
    else
       {
          document.getElementById(result).innerHTML="执行成功";
       }
  }

xmlhttp.open("POST", post_url, true);
// xmlhttp.setRequestHeader("X-CSRF-TOKEN", csrftoken);
xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xmlhttp.send(parameters);
}