from __future__ import unicode_literals
import datetime
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from projects.models import BuildTasks


# Create your models here.


@python_2_unicode_compatible
class ApprovalList(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.CharField(max_length=30, default='0')
    task = models.ForeignKey(BuildTasks, on_delete=models.CASCADE)
    title = models.CharField(max_length=300, default='0')
    approval_type = models.CharField(max_length=20, default='-')
    status = models.IntegerField(default=0)
    createtime = models.DateTimeField(auto_now_add=True)
    processtime = models.DateTimeField(null=True)
    
    def __str__(self):
        return self.message_id
    
    def list_save(self, user, title, approval_type, task_id):
        self.record = ApprovalList(user=user, title=title, approval_type=approval_type, task_id=task_id,
                                   createtime=datetime.datetime.now())
        self.record.save()
        if self.record.id:
            # print (self.record.id)
            return self.record.id
        else:
            return False

    def set_flags(self, task_id, approval_type, op, value):
        self.task_id = task_id
        self.approval_type = approval_type
        self.op = op
        self.value = value
        self.rs = ApprovalList.objects.get(task_id=self.task_id, approval_type=self.approval_type)
        if op == 'processed':
            self.rs.status = self.value
            self.rs.processtime = datetime.datetime.now()
            return self.rs.save()
        else:
            return 'Invalid Operation'

    def __str__(self):
        return self.id
