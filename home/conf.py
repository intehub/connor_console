from __future__ import print_function
from home.models import ApprovalList


class HomeConfig:
    def __init__(self):
        pass
    
    def get_approval_list_by_user(self, username):
        self.user = username
        self.approval_list = ApprovalList.objects.filter(user=self.user).order_by('-createtime')
        return self.approval_list

    def get_message_by_task_id(self, task_id, msg_type):
        self.task_id = task_id
        self.msg_type = msg_type
        self.message_detail = ApprovalList.objects.filter(task_id=self.task_id, message_type=self.msg_type)
        if self.message_detail and len(self.message_detail) == 1:
            return {
                'id': self.message_detail[0].id,
                'user': self.message_detail[0].user,
                'message_title': self.message_detail[0].message_title,
                'message_createtime': self.message_detail[0].message_createtime,
                'message_type': self.message_detail[0].message_type,
                'status': self.message_detail[0].status,
                'task_id': self.message_detail[0].task_id,
            }
        else:
            return False

    def unprocessed_msg_count(self):
        self.unprocessed_msg_count = ApprovalList.objects.filter(status=0).count()
        return self.unprocessed_msg_count
