from django.db.models import Count
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from projects.models import ServiceDefinition as pjServiceDefinition
from deployer.models import DeployTasks, ServiceDefinition as ServiceDefinition
from home.userlogin import UserLogin
from django.contrib.auth import authenticate, login, logout
from home.conf import HomeConfig
from .models import ApprovalList


# Create your views here.
@login_required(login_url='/login.html')
def index(request):
    
    services_envs = pjServiceDefinition.objects.all().values('namespace').annotate(env_count=Count('namespace')).annotate(env_count=Count('namespace'))
    services_count = pjServiceDefinition.objects.all().values('service_name').annotate(env_count=Count('service_name')).count()
    
    total_release = DeployTasks.objects.all().values('task_id').annotate(env_count=Count('task_id')).count()
    successful_release = DeployTasks.objects.filter(is_deployed=1).count()
    
    print (total_release)
    print (successful_release)
    
    unprocessed_count = HomeConfig().unprocessed_msg_count
    
    context = {'unprocessed_count': unprocessed_count, 'services_envs': services_envs, 'services_count': services_count,
               'successful_release': successful_release, 'total_release': total_release}
    return render(request, 'index.html', context)


@login_required(login_url='/login.html')
def helpcenter(request):
    return render(request, 'HelpCenter.html')


@login_required(login_url='/login.html')
def permissionmgt(request):
    return render(request, 'PermissionMgt.html')


def login_page(request):
    return render(request, 'login.html')


def add_user(request):
    user_creation = UserLogin().add_user('mediav', 'mediav123', 'paasadmin@mediav.com')
    if user_creation:
        return render(request, 'login.html')
    else:
        return render(request, 'login.html')


@login_required(login_url='/login.html')
def release_approval(request):
    if request.method == 'POST':
        pass
    else:
        username = request.user.username
        approval_list = ApprovalList.objects.select_related('task__service').filter(user=username)
        
        context = {'approval_list': approval_list}
        return render(request, 'ReleaseApproval.html', context)


def auth_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            # return render(request, 'index.html')
            return redirect('/index.html')
        else:
            return HttpResponse('user not logged in')
    else:
        return HttpResponse('welcome!')


def logout_view(request):
    logout(request)
    return redirect('/login.html')
