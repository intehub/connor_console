from __future__ import print_function
from deployer.k8s.conf import Config
from projects.conf import CIConfig
import pykube
import json
import os
import _jsonnet


class ServiceManifests:
	def __init__(self):
		# Set ingress code path
		self.ingress_code_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'k8s/config/'))
		
		# Init pykube client
		self.k8s_cluster_config = os.path.abspath(os.path.join(os.path.dirname(__file__), '../config/k8s/config'))
		self.api = pykube.HTTPClient(pykube.KubeConfig.from_file(self.k8s_cluster_config))
		
		self.config = Config()
		self.ciconfig = CIConfig()
	
	def worker(self, service_id, ingress_rule_name, service_deploy_name):
		# Init Parameters
		self.template_config_map = {}
		self.service_id = service_id
		self.ingress_rule_name = ingress_rule_name
		self.service_deploy_name = service_deploy_name
		
		# get service definition
		self.service_ci_detail = self.ciconfig.get_ci_service_by_service_id(self.service_id)
		self.service_cd_detail = self.config.get_service_config_by_serviceid(self.service_id)
		self.service_name = self.service_ci_detail['service_name']
		self.namespace = self.service_ci_detail['namespace']
		self.service_domain = self.service_ci_detail['service_domain']
		self.service_port = self.service_cd_detail['service_port']
		
		# Setup service hostname required by Ingress Rule
		self.service_hostname = self.ingress_rule_name + '.' + self.service_domain
		
		self.ingress_rule_exists = self.resource_exists(self.namespace, self.ingress_rule_name)
		
		if not self.ingress_rule_exists:
			if self.service_registration(self.service_id, self.ingress_rule_name, self.namespace, self.service_hostname, self.service_deploy_name, self.service_port):
				return True
			else:
				return False
		else:
			if self.service_update(self.service_id, self.ingress_rule_name, self.namespace, self.service_hostname, self.service_deploy_name, self.service_port):
				return True
			else:
				return False
	
	def resource_exists(self, namespace, ingress_rule_name):
		self.namespace = namespace
		self.ingress_rule_name = ingress_rule_name
		self.service_existence_ret = pykube.Ingress.objects(self.api).filter(namespace=self.namespace, field_selector={"metadata.name": self.ingress_rule_name})
		
		if self.service_existence_ret:
			print ('Ingress Rule For Service: ' + self.service_name + ' In Namespace: ' + self.namespace + ' Already Exists')
			return True
		else:
			print ('Ingress Rule Not Found')
			return False
		
	def service_registration(self, service_id, ingress_rule_name, namespace, service_hostname, service_deploy_name, service_port):
		# Init Parameters
		self.template_config_map = {}
		self.service_id = service_id
		self.ingress_rule_name = ingress_rule_name
		self.namespace = namespace
		self.service_hostname = service_hostname
		self.service_deploy_name = service_deploy_name
		self.service_port = service_port
		
		self.json_obj_map = self.fill_template(self.service_id, self.ingress_rule_name, self.namespace, self.service_hostname,
											self.service_deploy_name, self.service_port)
		
		# create ingress rule
		self.service_reg_ret = pykube.Ingress(self.api, self.json_obj_map['ingress']).create()
		
		if str(self.service_reg_ret) == 'None':
			print ('service registration successful')
			return True
		else:
			print ('service registration unsuccessful')
			return False
	
	def service_update(self, service_id, ingress_rule_name, namespace, service_hostname, service_deploy_name, service_port):
		# Init Parameters
		self.template_config_map = {}
		self.service_id = service_id
		self.ingress_rule_name = ingress_rule_name
		self.namespace = namespace
		self.service_hostname = service_hostname
		self.service_deploy_name = service_deploy_name
		self.service_port = service_port
		
		self.json_obj_map = self.fill_template(self.service_id, self.ingress_rule_name, self.namespace, self.service_hostname,
											self.service_deploy_name, self.service_port)
		
		# update ingress rule
		self.service_update_ret = pykube.Ingress(self.api, self.json_obj_map['ingress']).update()
		
		if str(self.service_update_ret) == 'None':
			print('Service update successful')
			return True
		else:
			print('Service update unsuccessful')
			return False
	
	def service_unregistartion(self, service_id, service_name, namespace, service_domain, service_deploy_name, service_port):
		# Init Parameters
		self.template_config_map = {}
		self.service_id = service_id
		self.service_name = service_name
		self.namespace = namespace
		self.service_domain = service_domain
		self.service_deploy_name = service_deploy_name
		self.service_port = service_port
		
		self.json_obj_map = self.fill_template(self.service_id, self.ingress_rule_name, self.namespace, self.service_hostname,
											self.service_deploy_name, self.service_port)
		
		# delete ingress rule
		self.service_unreg_ret = pykube.Ingress(self.api, self.json_obj_map['ingress']).delete()
		
		if str(self.service_unreg_ret) == 'None':
			print ('service un-registration successful')
			return True
		else:
			print ('service un-registration unsuccessful')
			return False
	
	def fill_template(self, service_id, ingress_rule_name, namespace, service_hostname, service_deploy_name, service_port):
		# Init Parameters
		self.template_config_map = {}
		self.service_id = service_id
		self.ingress_rule_name = ingress_rule_name
		self.namespace = namespace
		self.service_hostname = service_hostname
		self.service_deploy_name = service_deploy_name
		self.service_port = service_port
		
		self.config_map = {
							'ingress_rule_name': self.ingress_rule_name,
							'namespace': self.namespace,
							'service_hostname': self.service_hostname,
							'service_deploy_name': self.service_deploy_name,
							'service_port': self.service_port
							}
		
		# get service def in regards to ingress template to be used
		self.service_cd_detail = self.config.get_service_config_by_serviceid(self.service_id)
		self.ingress_script = os.path.join(self.ingress_code_path, self.service_cd_detail['service_ingress_template'])
		
		print (self.config_map)
		
		with open(self.ingress_script, 'r') as service_file:
			self.ingress_jsonnet_str = service_file.read()
			self.ingress_json_str = _jsonnet.evaluate_snippet("snippet", self.ingress_jsonnet_str, ext_vars=self.config_map)
			self.ingress_json_obj = json.loads(self.ingress_json_str)
		
		self.template_config_map['ingress'] = self.ingress_json_obj
		print (self.template_config_map['ingress'])
		
		return self.template_config_map