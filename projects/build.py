# coding:utf-8
from __future__ import print_function
import jenkins
import time
import json
from django.http import JsonResponse
from projects.conf import CIConfig
from projects.models import ServiceDefinition, BuildTasks, BuildTasksTransactions
from home.models import ApprovalList


class IntegrationBuilder:
    def __init__(self, request):
        # will put jenkins credential somewhere else later on
        self.message_subject_qarequest_prefix = None
        self.jenkins_username = 'jiestone'
        self.jenkins_password = '0p3n50urc3'
        self.email_from = 'connor@intehubtech.com'
        self.approval_subject_buildrequest_prefix = u"【通知】 发起构建和初始化部署流程 - 服务："
        self.approval_subject_qarequest_prefix = u"【通知】 初始化部署已完成，发起QA测试和审批流程 - 服务："
        self.my_username = request.user
        self.ciconfig = CIConfig()
        self.op_build_tasks_transactions = BuildTasksTransactions()

    def __str__(self):
        return self.service_name

    def task_creation(self, service_id, rollback_tag):
        self.service_id = service_id
        self.rollback_tag = rollback_tag
        print (self.my_username)
        
        # check if there's on-going CI Task in the queue for specific service
        self.status_requested = [0, 2]
        self.ci_task_running = CIConfig().get_ci_task_by_service_id(self.service_id, status=self.status_requested)
        if self.ci_task_running:
            print('Invalid Operation - There Is Build Task Running')
            return 2
        
        # Insert task into queue
        self.service_detail = CIConfig().get_ci_service_by_service_id(self.service_id)
        if self.service_detail:
            self.service_name = self.service_detail['service_name']
            self.namespace = self.service_detail['namespace']
            self.is_autotask = self.service_detail['is_autotask']
            
            # generate new release tag
            self.releasetag = self.generate_releasetag(self.service_name, self.namespace)
            self.insert_build_task = BuildTasks()

            self.ret = self.insert_build_task.task_record_save(service_id=self.service_id,
                                                               release_tag=self.releasetag,
                                                               rollback_tag=self.rollback_tag)
            
            if self.ret['id'] and self.ret['release_tag']:
                self.write_ci_logs(self.ret['id'], self.my_username, 'CI_Requested', u'构建任务写入成功')
                return 0
            else:
                return False
        else:
            print('Cannot Find service record')
            return False
    
    def write_ci_logs(self, ci_task_id, user, transact_type, transact_msg):
        self.ci_task_id = ci_task_id
        self.operator = user
        self.transact_type = transact_type
        self.transact_msg = transact_msg
        self.write_details_ret = self.op_build_tasks_transactions.transact_record_save(self.ci_task_id, self.operator, self.transact_type, self.transact_msg)
        if self.write_details_ret:
            print ('Writing To CI Transactions Successful')
            return True
        else:
            print ('Writing To CI Transactions Failed')
            return False
    
    def generate_releasetag(self, service_name, namespace):
        self.service_name = service_name
        self.namespace = namespace
        self.cur_time = time.strftime('%Y%m%d%H%M%S')
        self.releasetag = self.service_name + "-" + self.namespace + "-" + self.cur_time
        return self.releasetag

    def request_build(self, service_id, rollback_tag):
        self.service_id = service_id
        self.rollback_tag = rollback_tag
        
        # create task and get the task_id of the task we just created
        self.res = self.task_creation(self.service_id, self.rollback_tag)
        
        # create a message pending for approval, sent to corresponding person
        if self.res == 0:
            # sending request email to owner for approval
            self.service_detail = CIConfig().get_ci_service_by_service_id(self.service_id)
            self.service_name = self.service_detail['service_name']
            self.service_po = self.service_detail['service_po']
            self.service_po_email = self.service_detail['service_po_email']
            self.subject = self.approval_subject_buildrequest_prefix + self.service_name + u" - 版本号：" + \
                                                self.ret['release_tag']
            self.type = 'CI-Start'
            self.approval_id = ApprovalList().list_save(self.service_po, self.subject, self.type, self.ret['id'])
            
            self.ret = {'success': 'true', 'msg': u'操作成功 - 已创建构建任务'}
            return JsonResponse(self.ret)
        
        if self.res == 2:
            self.ret = {'success': 'false', 'msg': u'操作失败 - 有尚未完成的构建任务'}
            return JsonResponse(self.ret)
        
        if not self.res:
            self.ret = {'success': 'false', 'msg': u'操作失败 - 任务创建失败'}
            return JsonResponse(self.ret)
        
    def request_qa(self, task_id):
        self.task_id = task_id
        
        if self.task_id:
            # get corresponding CI task details
            self.res_ci_task_detail = CIConfig().get_ci_tasks_by_task_id(self.task_id)
            self.release_tag = self.res_ci_task_detail['release_tag']
            self.rollback_tag = self.res_ci_task_detail['rollback_tag']
            self.po_approved = self.res_ci_task_detail['po_approved']
            self.qa_approved = self.res_ci_task_detail['qa_approved']
            self.service_id = self.res_ci_task_detail['service_id']
    
            # get corresponding CI service details and trigger the build
            self.res_ci_service_detail = CIConfig().get_ci_service_by_service_id(self.service_id)
            self.service_qa = self.res_ci_service_detail['service_qa']
            self.service_name = self.res_ci_service_detail['service_name']
            self.job_name = self.res_ci_service_detail['job_name']
            self.current_release_tag = self.res_ci_service_detail['current_release_tag']
            
            self.subject = self.approval_subject_qarequest_prefix + self.service_name + u" - 版本号：" + self.release_tag
            self.type = 'QA-Start'

            self.approval_id = ApprovalList().list_save(self.service_qa, self.subject, self.type, self.task_id)
            
            if self.approval_id:
                return True
            else:
                return False

    def job_exists(self, jenkins_url, job_name):
        self.jenkins_url = jenkins_url
        self.job_name = job_name
        
        # Init Jenkins client
        self.jenkins_client = jenkins.Jenkins(self.jenkins_url, username=self.jenkins_username,
                                              password=self.jenkins_password)

        if self.jenkins_client.get_job_name(name=self.job_name):
            return True
        else:
            print('Job Not Found in Jenkins Server')
            return False

    def task_approved(self, task_id):
        self.task_id = task_id
        print(self.task_id)
        self.is_approved = BuildTasks.objects.filter(id=self.task_id, po_approved=1)
        if self.is_approved:
            return True
        else:
            print('This task is not approved yet')
            return False

    def worker(self, task_id):
        self.task_id = task_id
        
        # get corresponding CI task details
        self.res_ci_task_detail = CIConfig().get_ci_tasks_by_task_id(self.task_id)
        self.release_tag = self.res_ci_task_detail['release_tag']
        self.rollback_tag = self.res_ci_task_detail['rollback_tag']
        self.po_approved = self.res_ci_task_detail['po_approved']
        self.qa_approved = self.res_ci_task_detail['qa_approved']
        self.service_id = self.res_ci_task_detail['service_id']
        
        # get corresponding CI service details and trigger the build
        self.res_ci_service_detail = CIConfig().get_ci_service_by_service_id(self.service_id)
        self.jenkins_url = self.res_ci_service_detail['jenkins_url']
        self.service_name = self.res_ci_service_detail['service_name']
        self.job_name = self.res_ci_service_detail['job_name']
        self.current_release_tag = self.res_ci_service_detail['current_release_tag']
        
        self.jenkins_parameter = {'build_task_id': self.task_id, 'release_tag': self.release_tag}
        
        # Init Jenkins client
        self.jenkins_client = jenkins.Jenkins(self.jenkins_url, username=self.jenkins_username, password=self.jenkins_password)
        
        if self.po_approved == 1 and self.job_exists(self.jenkins_url, self.job_name) \
                and self.jenkins_client.build_job(self.job_name, token=self.jenkins_password,
                                                  parameters=self.jenkins_parameter) == '':

            print ('Trigger the job build successfully, waiting for job build to be completed')
            self.write_ci_logs(self.task_id, self.my_username, 'CI_Start', u'操作成功 - 成功提交构建任务')
            
            self.last_successful_build = self.jenkins_client.get_job_info(self.job_name)['lastCompletedBuild']['number']
            self.next_build_number = self.jenkins_client.get_job_info(self.job_name)['lastBuild']['number']
            
            # Waiting for job build to be completed successfully
            if self.watch_job_status(self.job_name, self.last_successful_build, self.next_build_number):
                # Send message to QA for smoke testing
                if self.request_qa(self.task_id):
                    self.set_status_flags = BuildTasks().set_flags(self.task_id, 'status', 1)
                    self.write_ci_logs(self.task_id, self.my_username, 'CI_Complete', u'操作成功 - 构建任务完成，已发送测试请求')
                    self.ret = {'success': 'True', 'msg': u'操作成功 - 构建任务完成，已发送测试请求'}
                    return JsonResponse(self.ret)
                else:
                    self.write_ci_logs(self.task_id, self.my_username, 'CI_Failed', u'操作失败 - 构建任务完成，发送测试请求失败')
                    self.ret = {'success': 'False', 'msg': u'操作失败 - 构建任务完成，发送测试请求失败'}
                    return JsonResponse(self.ret)
            else:
                self.set_status_flags = BuildTasks().set_flags(self.task_id, 'status', 2)
                print ('Build Failed')
                self.ret = {'success': 'False', 'msg': u'操作失败 - 构建任务失败'}
                return JsonResponse(self.ret)
        else:
            print ('Invalid Operation - This CI Task Is Not Approved By "Service Owner" Yet')
            self.write_ci_logs(self.task_id, self.my_username, 'CI_Start', u'操作失败 - 此任务尚未审批或未被同意发布')
            self.ret = {'success': 'false', 'msg': u'操作失败 - 此任务尚未审批或未被同意发布'}
            return JsonResponse(self.ret)
    
    def watch_job_status(self, job_name, last_successful_build, next_build):
        self.job_name = job_name
        self.last_successful_build = last_successful_build
        self.next_build = next_build

        # Waiting for job build to be completed successfully
        while True:
            self.last_successful_build = self.jenkins_client.get_job_info(self.job_name)['lastCompletedBuild']['number']
            if self.last_successful_build != self.next_build:
                print ('Waiting for new build ready...')
                time.sleep(10)
            else:
                break
        
        # Check the status of the job that was newly finished
        self.new_build_result = self.jenkins_client.get_build_info(self.job_name, self.next_build)['result']
        if self.new_build_result == 'SUCCESS':
            self.write_ci_logs(self.task_id, self.my_username, 'CI_Executing', u'操作成功 - 构建成功')
            return True
        else:
            self.write_ci_logs(self.task_id, self.my_username, 'CI_Executing', u'操作失败 - 构建失败')
            return False
    
    def request_approve(self, task_id):
        self.task_id = task_id

        # Update "Approve" flag, set to 1 for approval
        self.task_to_be_approved = BuildTasks()
        self.task_to_be_approved.set_flags(self.task_id, 'po_approved', 1)
        self.po_approved = self.task_to_be_approved.po_approved
        self.approval_type = 'CI-Start'
        
        self.approval_task_status = ApprovalList()
        self.approval_task_status.set_flags(self.task_id, self.approval_type, 'processed', 1)
        self.processed = self.approval_task_status.status

        if self.po_approved == 1:
            self.write_ci_logs(self.task_id, self.my_username, 'CI_Executing', u'操作成功 - 批准发布')
            self.ret = {'success': 'true', 'msg': u'操作成功'}
            return JsonResponse(self.ret)
        else:
            self.write_ci_logs(self.task_id, self.my_username, 'CI_Executing', u'操作失败 - 状态写入失败')
            self.ret = {'success': 'false', 'msg': u'操作失败'}
            return JsonResponse(self.ret)
    
    def request_refuse(self, task_id):
        self.task_id = task_id
        
        # Update "Approve" flag, set to 2 for refusal
        self.task_to_be_refused = BuildTasks()
        self.task_to_be_refused.set_flags(self.task_id, 'po_approved', 2)
        self.po_approved = self.task_to_be_refused.po_approved
        self.approval_type = 'CI-Start'
        self.ret = ApprovalList().set_flags(self.task_id, self.approval_type, 'processed', 1)

        if self.po_approved == 2 and self.ret:
            self.write_ci_logs(self.task_id, self.my_username, 'CI_Approval', u'操作成功 - 拒绝发布')
            self.ret = {'success': 'true', 'msg': u'操作成功'}
            return JsonResponse(self.ret)
        else:
            self.write_ci_logs(self.task_id, self.my_username, 'CI_Approval', u'操作失败 - 状态写入失败')
            self.ret = {'success': 'false', 'msg': u'操作失败'}
            return JsonResponse(self.ret)
    
    def qa_approve(self, task_id):
        self.task_id = task_id

        # Update "Approve" flag, set to 1 for approval
        self.task_to_be_approved = BuildTasks()
        self.task_to_be_approved.set_flags(self.task_id, 'qa_approved', 1)
        self.qa_approved = self.task_to_be_approved.qa_approved
        self.approval_type = 'QA-Start'
        self.ret = ApprovalList().set_flags(self.task_id, self.approval_type, 'processed', 1)

        if self.qa_approved == 1 and self.ret:
            self.write_ci_logs(self.task_id, self.my_username, 'QA_Approval', u'操作成功 - 批准上线')
            self.ret = {'success': 'true', 'msg': u'操作成功'}
            return JsonResponse(self.ret)
        else:
            self.write_ci_logs(self.task_id, self.my_username, 'QA_Approval', u'操作失败 - 状态写入失败')
            self.ret = {'success': 'false', 'msg': u'操作失败'}
            return JsonResponse(self.ret)
    
    def qa_refuse(self, task_id):
        self.task_id = task_id

        # Update "Approve" flag, set to 2 for refusal
        self.task_to_be_refused = BuildTasks()
        self.task_to_be_refused.set_flags(self.task_id, 'qa_approved', 2)
        self.qa_approved = self.task_to_be_refused.qa_approved
        self.approval_type = 'QA-Start'
        self.ret = ApprovalList().set_flags(self.task_id, self.approval_type, 'processed', 1)
        
        if self.qa_approved == 2 and self.ret:
            self.write_ci_logs(self.task_id, self.my_username, 'QA_Approval', u'操作成功 - 拒绝上线')
            self.ret = {'success': 'true', 'msg': u'操作成功'}
            return JsonResponse(self.ret)
        else:
            self.write_ci_logs(self.task_id, self.my_username, 'QA_Approval', u'操作失败 - 状态写入失败')
            self.ret = {'success': 'false', 'msg': u'操作失败'}
            return JsonResponse(self.ret)
        
    def ci_task_timeline(self, ci_task_id):
        self.ci_task_id = ci_task_id
        self.ci_task_timeline_res = self.ciconfig.get_ci_timeline_by_task_id(self.ci_task_id)
        return self.ci_task_timeline_res

