from __future__ import print_function
import os
import jenkins
from django.db.models import Q
from projects.models import ServiceDefinition, BuildTasks, BuildTasksTransactions


class CIConfig:
    def __init__(self):
        self.res = ''

    def get_ci_services(self):
        self.res_ci_services = ServiceDefinition.objects.all().order_by('service_name')
        return self.res_ci_services

    def get_ci_service_by_service_id(self, service_id, namespace='NULL'):
        self.service_id = service_id
        self.namespace = namespace

        if self.namespace == 'NULL':
            self.res = ServiceDefinition.objects.filter(id=self.service_id)
        else:
            self.res = ServiceDefinition.objects.filter(id=self.service_id, namespace=self.namespace)

        if self.res:
            return {
                'id': self.res[0].id,
                'namespace': self.res[0].namespace,
                'service_name': self.res[0].service_name,
                'jenkins_url': self.res[0].jenkins_url,
                'job_name': self.res[0].job_name,
                'current_release_tag': self.res[0].current_release_tag,
                'service_po': self.res[0].service_po,
                'service_po_email': self.res[0].service_po_email,
                'service_qa': self.res[0].service_qa,
                'service_qa_email': self.res[0].service_qa_email,
                'is_autotask': self.res[0].is_autotask,
                'service_domain': self.res[0].service_domain,
            }
        else:
            return False
    
    def get_ci_service_by_owner(self, username):
        self.owner = username
        self.ret = ServiceDefinition.objects.filter(Q(service_po=self.owner) | Q(service_qa=self.owner)).order_by('service_name')
        
        if self.ret:
            return self.ret
        else:
            return False

    def get_ci_task_by_service_id(self, service_id, limit=10, status=[]):
        self.service_id = service_id
        self.status = status
        self.limit = limit
        
        if not self.status:
            self.res = BuildTasks.objects.filter(service_id=self.service_id).order_by('-id')[:self.limit]
            return self.res
        else:
            self.res = BuildTasks.objects.filter(service_id=self.service_id, status__in=self.status).order_by('-id')
            return self.res
           
    def get_ci_tasks_by_task_id(self, task_id):
        self.task_id = task_id
        self.res_ci_task_detail = BuildTasks.objects.filter(id=self.task_id)
        
        if self.res_ci_task_detail:
            return {
                    'id': self.res_ci_task_detail[0].id,
                    'service_id': self.res_ci_task_detail[0].service_id,
                    'release_tag': self.res_ci_task_detail[0].release_tag,
                    'rollback_tag': self.res_ci_task_detail[0].rollback_tag,
                    'status': self.res_ci_task_detail[0].status,
                    'po_approved': self.res_ci_task_detail[0].po_approved,
                    'qa_approved': self.res_ci_task_detail[0].qa_approved,
            }
        else:
            return False

    def get_ci_releasetag_by_task_id(self, task_id):
        self.task_id = task_id
        if self.task_id:
            self.res_ci_task_id = BuildTasks.objects.filter(id=self.task_id)
            if self.res_ci_task_id and len(self.res_ci_task_id) == 1:
                return self.res_ci_task_id[0].release_tag
            else:
                return 0
        else:
            return 0

    def service_n_namespace_exists(self, service_name, namespace):
        self.service_name = service_name
        self.namespace = namespace
        self.ret = ServiceDefinition.objects.filter(service_name=self.service_name, namespace=self.namespace)
        
        if self.ret:
            return True
        else:
            return False
            
    def get_ci_timeline_by_task_id(self, ci_task_id):
        self.ci_task_id = ci_task_id
        self.timeline_res = BuildTasksTransactions.objects.filter(ci_task_id=self.ci_task_id).order_by('id')
        if self.timeline_res:
            return self.timeline_res
        else:
            return BuildTasksTransactions.objects.none()
