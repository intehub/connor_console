from __future__ import unicode_literals
from django.db import models
import datetime


# Create your models here.

class ServiceDefinition(models.Model):
    service_name = models.CharField(max_length=20)
    namespace = models.CharField(max_length=10, default='dev')
    jenkins_url = models.CharField(max_length=500)
    job_name = models.CharField(max_length=30)
    is_autotask = models.IntegerField(default=0)
    current_release_tag = models.CharField(max_length=200)
    service_po = models.CharField(max_length=20, default='')
    service_po_email = models.CharField(max_length=50, default='')
    service_qa = models.CharField(max_length=20, default='')
    service_qa_email = models.CharField(max_length=50, default='')
    service_domain = models.CharField(max_length=200, default='')

    def app_def_save(self, service_name, namespace, jenkins_url, job_name, is_autotask, current_release_tag, service_po,
                     service_po_email, service_qa, service_qa_email):
        self.record = ServiceDefinition(service_name=service_name, namespace=namespace, jenkins_url=jenkins_url,
                                        job_name=job_name, is_autotask=is_autotask,
                                        current_release_tag=current_release_tag,
                                        service_po=service_po, service_po_email=service_po_email,
                                        service_qa=service_qa, service_qa_email=service_qa_email,
                                        )
        self.record.save()
        if self.record.id:
            return self.record.id
        else:
            return False


class BuildTasks(models.Model):
    service = models.ForeignKey(ServiceDefinition)
    release_tag = models.CharField(max_length=200)
    rollback_tag = models.CharField(max_length=200, null=True)
    po_approved = models.IntegerField(default=0)
    po_approved_datetime = models.DateTimeField(null=True)
    qa_approved = models.IntegerField(default=0)
    qa_approved_datetime = models.DateTimeField(null=True)
    status = models.IntegerField(default=0)
    start_time = models.DateTimeField(auto_now=True)
    end_time = models.DateTimeField(null=True)
    
    def task_record_save(self, service_id, release_tag, rollback_tag):
        self.record = BuildTasks(service_id=service_id, release_tag=release_tag, rollback_tag=rollback_tag)
        self.record.save()
        if self.record.id and self.record.release_tag:
            self.ret = {'id': self.record.id, 'release_tag': self.record.release_tag}
            return self.ret
        else:
            return False
    
    # 1 = success / approval, 2 = failure / refusal
    def set_flags(self, task_id, op, value):
        self.task_id = task_id
        self.op = op
        self.value = value
        self.rs = BuildTasks.objects.get(id=self.task_id)
        if op == 'po_approved':
            self.rs.po_approved = self.value
            self.rs.po_approved_datetime = datetime.datetime.now()
            return self.rs.save()
        elif op == 'qa_approved':
            self.rs.qa_approved = self.value
            self.rs.qa_approved_datetime = datetime.datetime.now()
            return self.rs.save()
        elif op == 'status':
            self.rs.status = self.value
            self.rs.end_time = datetime.datetime.now()
            return self.rs.save()
        else:
            print ('Invalid Operation')
            return False


class BuildTasksTransactions(models.Model):
    id = models.AutoField(primary_key=True)
    ci_task_id = models.IntegerField(default=-1)
    operator = models.CharField(max_length=200)
    transact_type = models.CharField(max_length=15)
    transact_msg = models.CharField(max_length=500)
    transact_datetime = models.DateTimeField(null=True)
    
    def transact_record_save(self, ci_task_id, operator, transact_type, transact_msg):
        self.record = BuildTasksTransactions(ci_task_id=ci_task_id, operator=operator, transact_type=transact_type,
                                             transact_msg=transact_msg, transact_datetime=datetime.datetime.now())
        self.record.save()
        if self.record.id:
            return True
        else:
            return False
