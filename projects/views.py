# -*- coding: utf-8 -*-
import json
import build
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required


@login_required(login_url='/login.html')
@csrf_exempt
def build_request(request):
    if request.method == 'POST':
        build_data = json.loads(request.body)
        try:
            service_id = build_data['service_id']
            rollback_tag = build_data['rollback_tag']
            build_req = build.IntegrationBuilder(request)
            build_req_res = build_req.request_build(service_id, rollback_tag)
            return HttpResponse(build_req_res)
        except KeyError:
            return HttpResponse("Malformed data!")
    elif request.method == 'GET':
        return HttpResponse('welcome!')


@login_required(login_url='/login.html')
@csrf_exempt
def jobs_execute(request):
    if request.method == 'POST':
        build_data = json.loads(request.body)
        try:
            build_task_id = build_data['build_task_id']
            builder = build.IntegrationBuilder(request)
            build_res = builder.worker(build_task_id)
            return HttpResponse(build_res)
        except KeyError:
            return HttpResponse("Malformed data!")
    elif request.method == 'GET':
        return HttpResponse('welcome!')


@login_required(login_url='/login.html')
@csrf_exempt
def watch_job_status(request):
    if request.method == 'POST':
        task_id = request.POST['task_id']
        request_approver = build.IntegrationBuilder(request)
        request_approver_res = request_approver.request_approve(task_id)
        return HttpResponse(request_approver_res)
    elif request.method == 'GET':
        return HttpResponse('welcome!')


@login_required(login_url='/login.html')
@csrf_exempt
def build_request_approve(request):
    if request.method == 'POST':
        build_data = json.loads(request.body)
        try:
            ci_task_id = build_data['ci_task_id']
            request_approver = build.IntegrationBuilder(request)
            request_approver_res = request_approver.request_approve(ci_task_id)
            return HttpResponse(request_approver_res)
        except KeyError:
            return HttpResponse("Malformed data!")
    elif request.method == 'GET':
        return HttpResponse('welcome!')


@login_required(login_url='/login.html')
@csrf_exempt
def build_request_refuse(request):
    if request.method == 'POST':
        build_data = json.loads(request.body)
        try:
            ci_task_id = build_data['ci_task_id']
            request_refuser = build.IntegrationBuilder(request)
            request_refuser_res = request_refuser.request_refuse(ci_task_id)
            return HttpResponse(request_refuser_res)
        except KeyError:
            return HttpResponse("Malformed data!")
    elif request.method == 'GET':
        return HttpResponse('welcome!')


@login_required(login_url='/login.html')
@csrf_exempt
def build_qa_approve(request):
    if request.method == 'POST':
        build_data = json.loads(request.body)
        try:
            ci_task_id = build_data['ci_task_id']
            qa_approver = build.IntegrationBuilder(request)
            qa_approver_res = qa_approver.qa_approve(ci_task_id)
            return HttpResponse(qa_approver_res)
        except KeyError:
            return HttpResponse("Malformed data!")
    elif request.method == 'GET':
        return HttpResponse('welcome!')


@login_required(login_url='/login.html')
@csrf_exempt
def build_qa_refuse(request):
    if request.method == 'POST':
        build_data = json.loads(request.body)
        try:
            ci_task_id = build_data['ci_task_id']
            qa_refuser = build.IntegrationBuilder(request)
            qa_refuser_res = qa_refuser.qa_refuse(ci_task_id)
            return HttpResponse(qa_refuser_res)
        except KeyError:
            return HttpResponse("Malformed data!")
    elif request.method == 'GET':
        return HttpResponse('welcome!')


@login_required(login_url='/login.html')
@csrf_exempt
def status_details(request):
    if request.method == 'POST':
        ci_task_id = request.POST['ci_task_id']
        builder = build.IntegrationBuilder(request)
        ci_task_timeline = builder.ci_task_timeline(ci_task_id)
        context = {'ci_task_timeline': ci_task_timeline}
        return render(request, 'CITimeLine.html', context)
    elif request.method == 'GET':
        return HttpResponse('welcome!')
