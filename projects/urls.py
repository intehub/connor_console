from django.conf.urls import url

from . import views

app_name = 'projects'

urlpatterns = [
    url(r'^request$', views.build_request, name='build_request'),
    url(r'^execute$', views.jobs_execute, name='jobs_execute'),
    url(r'^request/approve$', views.build_request_approve, name='build_request_approve'),
    url(r'^request/refuse$', views.build_request_refuse, name='build_request_refuse'),
    url(r'^qa/approve$', views.build_qa_approve, name='build_qa_approve'),
    url(r'^qa/refuse$', views.build_qa_refuse, name='build_qa_refuse'),
    url(r'^watch-job-status$', views.watch_job_status, name='watch_job_status'),
    url(r'^task/StatusDetails.html$', views.status_details, name='status_details'),
]
