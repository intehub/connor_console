FROM centos:7
MAINTAINER Tony Zhang <opsguruzj@gmail.com>

RUN yum -y update
RUN yum -y install python python-devel gcc epel-release 
RUN yum -y install python-pip
RUN pip install --upgrade pip
RUN pip install uwsgi
RUN pip install Django

RUN pip install -r requirements.txt

RUN useradd -s /bin/bash app_admin
RUN mkdir /home/app_admin/webapps

WORKDIR /var/lib/jenkins/workspace/connor-console/
ADD . /home/app_admin/webapps/

WORKDIR /home/app_admin/webapps
USER app_admin

# EXPOSE 1118

# ENTRYPOINT /usr/bin/uwsgi --http :1118 --wsgi-file test.py -w test:app --master
